var timer = function(name) {
    var start = new Date();
    return {
        stop: function() {
            var end  = new Date();
            var time = end.getTime() - start.getTime();
            print('Timer: ' + name + ' finished in ' + time + 'ms');
        }
    }
};


var l = java.util.ArrayList();
var tmp;
for (var i=0; i<1000000; i++) l.add(i);

var t = timer('index access');
for (var len = l.size(), i = 0; i< len; i++ ) {
  tmp =l.get(i); 
}
t.stop();

var t = timer('iterator access');
for (var iterator = l.iterator(); iterator.hasNext();  ) {
  tmp = iterator.next();   
}
t.stop();